from math import exp, log, log1p

if __name__ == '__main__':
    lnp = 0
    lnq = -36
    lnr = -746

    '''
    p = exp(lnp)
    q = exp(lnq)
    r = exp(lnr) # r = 0.0  log(0) is undefined
    '''

    # log(pqr) = log(p) + log(q) + log(r)
    a = lnp + lnq + lnr
    print("log(pqr) = " + str(a))

    # log(p + q + r) = log(p) + log1p(x)
    # x = exp(log(q) - log(p)) + exp(log(r) - log(p))
    x = exp(lnq - lnp) + exp(lnr - lnp)
    b = lnp + log1p(x)
    print("log(p + q + r) = " + str(b))

    # log(q^21 + (r / 746))
    # log(x + y) = log(x) + log1p(exp(log(y) - log(x)))
    # log(q^21) = 21 * log(q)
    # log(r / 746) = log(r) - log(746)
    # log(r / (746 * q^21)) = log(r) - log(746 * q^21) = log(r) - log(746) - 21 * log(q)
    #
    # log(q^21 + (r/746)) = 21 * log(q) + log1p(exp(log(r) - log(746) - 21 * log(q)))
    c = 21 * lnq + log1p(exp(lnr - log(746) - 21 * lnq))
    print("log(q^21 + r/746) = " + str(c))
