import numpy as np
import string

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import simulation.simulation as simulation


def distribution(alphabet):
    """Returns the distribution of the alphabet.

    p = 1 / (i * i)

    :param alphabet: The alphabet
    :return: The distribution of the symbols
    """
    p = []
    for i in range(1, len(alphabet) + 1):
        p.append(1 / (i ** 2))
    return np.array(p) / sum(p)


if __name__ == '__main__':
    alphabet = string.digits + string.ascii_uppercase + string.ascii_lowercase
    text_length = 10000  # 0000

    sim = simulation.TextSimulation(alphabet, text_length)
    # 1. Simuliere einen Text der Laenge 100 Millionen Zeichen gemaess der Gleichverteilung des Alphabets.
    print(sim.simulate()[:100])  # Gibt die ersten 100 Zeichen aus.

    # 2. Bestimme die Verteilung p = 1 / (i * i) von 1 bis 62 explizit. Wie gross ist jeweils p(0), p(A), p(a), p(z)?
    p = distribution(alphabet)
    print("p0: " + str(p[alphabet.index("0")]))
    print("pA: " + str(p[alphabet.index("A")]))
    print("pa: " + str(p[alphabet.index("a")]))
    print("pz: " + str(p[alphabet.index("z")]))

    # 3. Simuliere einen Text der Länge 100 Millionen Zeichen gemaess p mit linearer Intervallsuche.
    print(sim.simulate(p, "linear")[:100])  # Gibt die ersten 100 Zeichen aus.

    # 4. Bestimme die kumulative Verteilungsfunktion P zu p.
    print(p.cumsum())

    # 5. Simuliere einen Text der Länge 100 Millionen Zeichen gemaess p mit binaerer Intervallsuche.
    print(sim.simulate(p, "binary")[:100])  # Gibt die ersten 100 Zeichen aus.

    # 5. Bestimme die Alias-Tabelle zu p
    print(simulation.alias_table(p))

    # 6. Simuliere einen Text der Länge 100 Millionen Zeichen gemaess p mit der Alias Methode.
    print(sim.simulate(p, "alias")[:100])  # Gibt die ersten 100 Zeichen aus.
