import numpy as np

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import paa.paa as p

if __name__ == '__main__':
    # Aufgabe 7.1
    states = np.array(range(6))
    print(states)
    epsilon = np.array([1 / 6] * 6)
    print(epsilon)

    # Aufgabe 7.2
    states = np.array(range(3))  # 0: erschoepft, 1: wach, 2: hochkonzentriert
    transition = np.array([[0.9, 0.1, 0.0],  # erschoepft
                           [0.05, 0.9, 0.05],  # wach
                           [0.0, 0.1, 0.9]])  # hochkonzentriert
    epsilon = np.array([0.5, 0.1, 0.02])
    start_state = 1  # wach
    values = np.array(range(2))  # 0: falsch, 1: richtig
    p.PAA(states, start_state, transition, values, values[1], epsilon, None)

    # Aufgabe 7.3
