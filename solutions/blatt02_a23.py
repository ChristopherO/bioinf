import numpy as np
import numpy.linalg as npl

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import textmodels.text_model as tm

if __name__ == '__main__':
    # Aufgabe 2.2
    s = "GATTACA"
    t = "ACATTAG"
    u = "TACG" * 800  # 800 times TACG

    print(s)
    print(t)
    print(u)

    # Aufgabe 2.2.1
    # legend for indexing
    print()
    legend = {"A": 0,
              "C": 1,
              "G": 2,
              "T": 3}
    model_s = tm.TextModel(s, legend)
    model_t = tm.TextModel(t, legend)
    model_u = tm.TextModel(u, legend)

    print("M00:")
    print(model_s.m00(4))
    print(model_t.m00(4))
    print(model_u.m00(4))
    print("M00 (log):")
    print(model_s.m00(4, True))
    print(model_t.m00(4, True))
    print(model_u.m00(4, True))

    # Aufgabe 2.2.2
    print()
    p = np.array([1 / 6, 2 / 6, 2 / 6, 1 / 6])
    print("M0:")
    print(model_s.m0(p))
    print(model_t.m0(p))
    print(model_u.m0(p))
    print("M0 (log):")
    print(model_s.m0(p, True))
    print(model_t.m0(p, True))
    print(model_u.m0(p, True))

    # Aufgabe 2.2.3
    print()
    start = np.array([1 / 4] * 4)
    transition = np.array([[1 / 6, 2 / 6, 1 / 6, 2 / 6],
                           [2 / 6, 1 / 6, 2 / 6, 1 / 6],
                           [1 / 6, 2 / 6, 1 / 6, 2 / 6],
                           [2 / 6, 1 / 6, 2 / 6, 1 / 6]])
    print("M1:")
    print(model_s.m1(start, transition))
    print(model_t.m1(start, transition))
    print(model_u.m1(start, transition))
    print("M1 (log):")
    print(model_s.m1(start, transition, True))
    print(model_t.m1(start, transition, True))
    print(model_u.m1(start, transition, True))

    # Aufgabe 2.3
    print()
    vec = np.zeros(5)
    vec[4] = 1.0
    trans_t = transition.T - np.identity(4)
    trans_t = np.append(trans_t, [[1, 1, 1, 1]], axis=0)
    sol = npl.lstsq(trans_t, vec)
    print("Solution: " + str(sol[0]))
    print("Check solution: " + str(np.allclose(np.dot(transition, np.array(sol[0])), np.array(sol[0]))))
