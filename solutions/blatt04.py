import numpy as np
import matplotlib.pyplot as plt

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import simulation.simulation as sim
import hmm.hmm as hmm


def simulate(transition, emissions):
    """Simulates a row of throws.

    :param transition: The transition matrix
    :param emissions: The emissions
    """
    for i in range(100):
        filename = "simulation" + str(i) + ".txt"
        observations = simulate_throw(transition, emissions)
        with open(filename, 'w') as f:
            # f.write("state observation\n")
            for state, ob in observations:
                f.write(str(state) + " " + str(ob) + "\n")


def simulate_throw(transition, emissions):
    """Simulations a throw based on the transition matrix and the emissions of the cubes.

    :param transition: The transition matrix
    :param emissions: The emissions
    :return: The simulated throw
    """
    observations = []
    # observations = []
    state = 0  # begin with fair cube

    observations.append((state, sim.linear_search(emissions[state])))
    for i in range(999):
        state = sim.linear_search(transition[state])
        observations.append((state, sim.linear_search(emissions[state])))
    return observations


def load_simulations():
    simulations = []
    for i in range(100):
        states = []
        observations = []
        filename = "simulation" + str(i) + ".txt"
        with open(filename, 'r') as f:
            for line in f:
                line = line.rstrip()
                ob = line.split(" ")
                states.append(int(ob[0]))
                observations.append(int(ob[1]))
        simulations.append((states, observations))
    return simulations


if __name__ == '__main__':
    states = range(3)  # 0 : F, 1 : U, 2 : A

    prob_f = np.full((6), 1 / 6)
    prob_u = np.full((6), 1 / 5)
    prob_u[5] = 0.0
    prob_a = np.full((6), 1 / 7)
    prob_a[5] = 2 / 7
    emissions = np.array([prob_f, prob_u, prob_a])

    start = np.array([1.0, 0.0, 0.0])
    transition = np.array([[0.8, 0.1, 0.1],
                           [0.2, 0.8, 0.0],
                           [0.2, 0.0, 0.8]])

    # Aufgabe 4.2
    #simulate(transition, emissions)

    # Aufgabe 4.3
    simulations = load_simulations()

    markov = hmm.HMM(states, start, transition, emissions)
    fws = [markov.forward(ob) for _, ob in simulations]
    bws = [markov.backward(ob) for _, ob in simulations]
    # assert fws == bws
    print(fws)
    print(bws)

    plt.hist(fws)
    plt.xlabel('Wahrscheinlichkeiten')
    plt.ylabel('Häufigkeiten')
    plt.title('Wahrscheinlichkeiten der Beobachtungsreihen')
    plt.show()

    # Aufgabe 4.4
    viterbi_paths = [markov.viterbi(ob) for _, ob in simulations]
    print(viterbi_paths)

    # Aufgabe 4.5
    print([markov.compare_paths(simulations[i][0], viterbi_paths[i][1]) for i in range(len(viterbi_paths))])
