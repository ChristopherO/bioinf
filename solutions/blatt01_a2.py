import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from dna import fasta, codons
from simulation import simulation

if __name__ == '__main__':
    # Aufgabe 1.2.1
    file_name = "ecoli_K12_MG1655_genes.fa"
    sequences = fasta.read_sequences(file_name)

    print("Number of sequences: " + str(len(sequences)))
    print("Lengths of the sequences: " + str([len(seq) for seq in sequences]))
    print("All sequence lengths divisible by 3: " + str(codons.check_length(sequences)))
    print("All sequences start with 'ATG' or 'GTG': " + str(codons.start_codon(sequences)))

    # Aufgabe 1.2.2
    print()
    occurrences = codons.count_condons(sequences)
    distribution = occurrences['occurrences'] / occurrences["occurrences"].sum()  # normalize distribution
    print("Number of codons: " + str(len(occurrences[occurrences["occurrences"] > 0])))
    maximum_occurrence = occurrences.loc[occurrences["occurrences"].idxmax()]  # get entries with maximum occurrences
    print("Maximum occurrence: " + str(maximum_occurrence["codons"]) + ": " + str(maximum_occurrence["occurrences"]))
    print("Codons with probability of exact zero or near zero:")
    print(occurrences.loc[distribution < 0.001])  # get entries with probability near or exact zero
    print(occurrences.sort_values(by="occurrences"))

    # Aufgabe 1.2.3
    print()
    sequence_len = 90
    cdns = occurrences["codons"]
    sim = simulation.TextSimulation(cdns, sequence_len - 3)
    print("Simulated sequence:")
    print("ATG" + sim.simulate(distribution, "linear"))  # simulate a sequence of length len - 3 and add to start codon
