import numpy as np


class PAA:

    def __init__(self, states, start_state, transition, values, start_value, epsilon, mu):
        self.states = states
        self.m = len(states)
        self.start_state = start_state
        self.transition = transition
        self.values = values
        self.V = len(values)
        self.start_value = start_value
        self.epsilon = epsilon
        self.mu = mu

    def recurrence(self, observation):
        n = len(observation)
        f = np.zeros((n, self.m, self.V))
        for t in range(n):
            for q in range(self.m):
                for v in range(len(self.values[t - 1])):
                    for q_2 in range(self.m):
                        for e in range(self.epsilon):
                            v_tmp = v + e
                            f[t, q_2, v_tmp] += f[t-1, q, v] * self.transition[q, q_2] * self.mu[q_2, e]
        return f[n-1]

    @property
    def __str__(self):
        return "States: " + str(self.states) + "\nStart state: " + str(self.start_state) + "\nTransition: " + str(
            self.transition) + "\nValues: " + str(self.values) + "\nStart value: " + str(
            self.start_value) + "\nEmissions: " + str(self.epsilon) + "\nEmission distribution: " + str(self.mu)
