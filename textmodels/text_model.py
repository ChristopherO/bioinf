from math import log, exp, log1p


class TextModel:
    def __init__(self, string, legend):
        self.string = string
        self.legend = legend

    def m00(self, alphabet_size, lg=False):
        """Calculates the M00 distribution for the given text.

        :param alphabet_size: The size of the alphabet
        :param lg: True for logarithmic scaling
        :return: The M00 distribution of the text
        """
        if alphabet_size == 0:
            return -1
        m = 1
        if lg:
            m = 0
        probability = 1 / alphabet_size
        for i in range(len(self.string)):
            if lg:
                m += log(probability)
            else:
                m *= probability
        return m

    def m0(self, p, lg=False):
        """Calculates the M0 distribution for the given text.

        :param p: The probabilities
        :param lg: True for logarithmic scaling
        :return: The M0 distribution of the text
        """
        m = 1
        if lg:
            m = 0
        for s in self.string:
            if lg:
                m += log(p[self.legend[s]])
            else:
                m *= p[self.legend[s]]
        return m

    def m1(self, start, transition, lg=False):
        """Calculates the M1 distribution for the given text.

        :param start: The start distribution
        :param transition: The transition matrix
        :param lg: True for logarithmic scaling
        :return: The M1 distribution of the text
        """
        if lg:
            m = log(start[self.legend[self.string[0]]])
        else:
            m = start[self.legend[self.string[0]]]
        for s in range(0, len(self.string) - 1):
            if lg:
                m += log(transition[self.legend[self.string[s]], self.legend[self.string[s + 1]]])
            else:
                m *= transition[self.legend[self.string[s]], self.legend[self.string[s + 1]]]
        return m
