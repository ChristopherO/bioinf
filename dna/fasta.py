

def read_sequences(file_name):
    """Reads all sequences of the FASTA file, ignores all headers.

    :param file_name: The file to read in
    :return: A list of all sequences contained in the file
    """
    seqs = []
    with open(file_name, 'r') as f:
        seq = ""
        for line in f:
            # Expand sequence if the given line is no header
            if not line.startswith(">"):
                seq += line.rstrip()
            else:  # Header line, sequence read
                if seq:
                    seqs.append(seq)
                    seq = ""
        seqs.append(seq)  # add last sequence
    return seqs


def read_fasta(file_name):
    """Reads all sequences of the FASTA file.

    :param file_name: The file to read in
    :return: A list of tuples of header and sequence contained in the file
    """
    seqs = []
    with open(file_name, 'r') as f:
        header = ""
        seq = ""
        for line in f:
            # Expand sequence if the given line is no header
            if not line.startswith(">"):
                seq += line.rstrip()
            else:  # Header line, sequence read
                if seq:
                    seqs.append((header, seq))
                    seq = ""
                header = line
        seqs.append(seq)  # add last sequence
    return seqs
