import pandas as pd


def check_length(sequences):
    """Checks whether the lengths of all sequences are divisible by 3.

    :param sequences: The sequences
    :return: True if the lengths of all sequences are divisible by 3
    """
    return all([len(seq) % 3 == 0 for seq in sequences])


def start_codon(sequences):
    """Checks whether all sequences start with one of the start codons 'ATG' or 'GTG'.

    :param sequences: The sequences
    :return: True if all sequences start with a start codon
    """
    return all([seq.startswith("ATG") or seq.startswith("GTG") for seq in sequences])


def gencodons():
    """Generates a dictionary containing all possible codons.

    :return: A dictionary of all possible codons initialized with zero.
    """
    codons = dict()
    acgt = ['A', 'C', 'G', 'T']
    for i in acgt:
        for j in acgt:
            for k in acgt:
                codons[i + j + k] = 0
    return codons


def count_condons(sequences):
    """Counts the occurrences of each codon within the sequences.

    :param sequences: The sequences
    :return: The occurrences of each codon
    """
    """Counts the number of each codon.

    Args:
        seqs (list[str]): A list of sequences

    Returns:
        Dataframe: A pandas Dataframe containing the distribution of the codons. The distribution is NOT normalized.
    """
    cdns = gencodons()
    for seq in sequences:
        i = 3
        while i < len(seq):
            cdns[seq[i:i + 3]] = cdns[seq[i:i + 3]] + 1
            i += 3
    keys = sorted(cdns)
    values = []
    for key in keys:
        values.append(cdns[key])
    df = pd.DataFrame({
        'codons': keys,
        'occurrences': values
    })
    return df
