import numpy as np

C =np.array([
    [1,28,0,2],
    [4,5,0,22],
    [1,5,4,21],
    [2,0,0,29],
    [0,3,25,4],
    [17,3,1,10],
    [13,2,7,9],
    ],
    dtype=np.float64
).T

print(C)
P = (C + 1.0)
s = np.sum(P,axis=0)
s

P = P/s
P

S = np.round(10*np.log2( P / (1/4)))
S

np.sum(np.max(S, axis=0))

P = C / np.sum(C,axis=0)
S = np.log2( P / (1/4))
H = P * S
h = np.nansum(H, axis=0)
h

def permuted_lookahead(w, S, pi, M, T):
    """
        S Matrix
        pi permutation
        M maxima die noch erreichbar sind
        T Treshhold
        w Wort
    """
    length = len(w)
    s = 0
    for j in range(length):
        s += S[w[pi[j]],pi[j]]
        if s + M[j] < T:
            return j+1
    return length    
    

def get_pi_opt(S, h):
    return np.argsort(h)[::-1]

def get_pi_LR(S,h):
    length = S.shape[1]
    return np.array(range(length), dtype=int)

def get_pi_RL(S,h):
    length = S.shape[1]
    return np.array(range(length), dtype=int)[::-1]

def get_pi_worst(S,h):
    return np.argsort(h)


get_pi = dict(opt=get_pi_opt, LR=get_pi_LR, RL=get_pi_RL, worst=get_pi_worst)

def get_M(S, pi):
    length= S.shape[1]
    M = np.zeros(length, dtype=np.float64)
    for i in range(length):
        M[i] = sum(np.max(S[:,pi[j]]) for j in range(i+1, length))
    return M

def simulate_permuted_lookahead(C, T, n=1000000, strategy="opt"):
    P = C / np.sum(C,axis=0)
    S = np.round(10*np.log2( P / (1/4)))
    H = P * S
    h = np.nansum(H, axis=0)
    pi = get_pi[strategy](S,h)
    M = get_M(S, pi)
    print(pi)
    print(M)
    length = C.shape[1]
    randint = np.random.randint
    total = 0
    dist = np.zeros(length+1)
    for t in range(n):
        w = randint(4, size=length)
        steps = permuted_lookahead(w, S, pi, M, T)
        total += steps
        dist[steps] += 1
    return (total / n), (dist / np.sum(dist))

mean, dist = simulate_permuted_lookahead(C, 56.0, strategy="worst")
print("mean: {:.2f}".format(mean))
print("distribution: ", dist[1:])


# 6.3 Keine Lust zu implementieren