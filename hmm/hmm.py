import numpy as np
from math import log1p, log, exp

from blatt04.blatt04 import forward_algorithm


class HMM:
    def __init__(self, states, start, transition, emissions, lg=True):
        """Constructor

        :param states: The states of the HMM
        :param start: The start distribution
        :param transition: The transition matrix
        :param emissions: The emissions for each state
        :param lg: True for logarithmic scaling
        """

        self.states = states
        self.n = len(states)
        if lg:
            start = np.log(start.clip(min=0.0000000001))
            transition = np.log(transition.clip(min=0.0000000001))
            emissions = np.log(emissions.clip(min=0.0000000001))
        self.start = start
        self.transition = transition
        self.emissions = emissions
        self.lg = lg

    def forward(self, observations):
        """Applies the forward algorithm for the HMM with the given observations.

        :param observations: The observations in form of the indices of the observed states

        :return:
        """
        m = len(observations)
        fw = np.zeros((self.n, m))
        if self.lg:
            for i in range(self.n):
                fw[i, 0] = self.start[i] + self.emissions[i, observations[0]]
            for t in range(1, m):
                for i in range(self.n):
                    fw[i, t] = logsum([fw[j, t - 1] + self.transition[j, i] for j in range(self.n)]) + self.emissions[
                        i, observations[t]]
            return logsum([fw[i, m - 1] for i in range(self.n)])
        else:
            for i in range(self.n):
                fw[i, 0] = self.start[i] * self.emissions[i, observations[0]]
            for t in range(1, m):
                for i in range(self.n):
                    fw[i, t] = sum([fw[j, t - 1] * self.transition[j, i] for j in range(self.n)]) * self.emissions[
                        i, observations[t]]
            return sum([fw[i, m - 1] for i in range(self.n)])

    def backward(self, observations):
        """Applies the backward algorithm for the HMM with the given observations.

        :param observations: The observations in form of the indices of the observed states
        :return:
        """
        m = len(observations)
        bw = np.zeros((self.n, m))  # log(1) = 0
        if self.lg:
            for t in range(m - 2, 0, -1):
                for i in range(self.n):
                    bw[i, t] = logsum(
                        [bw[j, t + 1] + self.transition[i, j] + self.emissions[j, observations[t + 1]] for j in
                         range(self.n)])
            return logsum([bw[k, 0] * self.emissions[k, observations[0]] * self.start[k] for k in range(self.n)])
        else:
            for i in range(self.n):
                bw[i, m - 1] = 1
            for t in range(m - 2, 0, -1):
                for i in range(self.n):
                    bw[i, t] = sum(
                        [bw[j, t + 1] * self.transition[i, j] * self.emissions[j, observations[t + 1]] for j in
                         range(self.n)])
            return sum([bw[i, 0] * self.emissions[i, observations[0]] * self.start[i] for i in range(self.n)])

    def forward_backward(self, observations):
        fwd = self.forward(observations)
        bwd = self.backward(observations)
        fb = np.zeros(self.n)
        for i in range(len(observations)):
            for s in range(self.n):
                fb[s] = fwd[i][s] * bwd[i][s]
        return fb

    def viterbi(self, observations):
        """

        :param observations: The observations
        :return:
        """
        m = len(observations)
        vit = np.zeros((self.n, m))
        path = dict()
        for i in self.states:
            if self.lg:
                vit[i, 0] = self.start[i] + self.emissions[i, observations[0]]
            else:
                vit[i, 0] = self.start[i] * self.emissions[i, observations[0]]
            path[i] = [i]
        for t in range(1, m):
            newpath = dict()
            for i in self.states:
                if self.lg:
                    (prob, state) = max(
                        (vit[j, t - 1] + self.transition[j, i] + self.emissions[i, observations[t]], j) for j in
                        range(self.n))
                else:
                    (prob, state) = max(
                        (vit[j, t - 1] * self.transition[j, i] * self.emissions[i, observations[t]], j) for j in
                        range(self.n))
                    vit[i, t] = prob
                    newpath[i] = path[state] + [i]
                    path = newpath
                    (prob, state) = max((vit[i, m - 1], i) for i in self.states)
        return prob, path[state]

    def compare_paths(self, real_states, viterbi_path):
        return len([i for i, j in zip(real_states, viterbi_path) if i == j])


def logsum(x):
    """Calculates the logarithmic sum of x.

    Note that the elements in x are expected to be already logarithmic scaled.
    :param x: The elements to calculate the logarithmic sum for

    :return: The logarithmic sum
    """
    ibig = np.argmax(x)
    y = x[:ibig]
    z = x[ibig + 1:]
    big = x[ibig]
    s = np.sum(np.exp(y - big)) + np.sum(np.exp(z - big))
    return big + log1p(s)
