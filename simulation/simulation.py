import random


class TextSimulation:
    def __init__(self, alphabet, text_len):
        self.alphabet = alphabet
        self.text_len = text_len

    def simulate(self, p=None, mode="uniform"):
        """

        :param p:
        :param mode:
        :return:
        """
        text = ""
        alphabet_len = len(self.alphabet)
        if mode == "uniform":
            for i in range(self.text_len):
                text += self.alphabet[random.randrange(alphabet_len)]
        elif mode == "linear":
            for i in range(self.text_len):
                text += self.alphabet[linear_search(p)]
        elif mode == "binary":
            for i in range(self.text_len):
                text += self.alphabet[binary_search(p)]
        elif mode == "alias":
            (a, q) = alias_table(p)
            for j in range(self.text_len):
                i = random.randint(0, self.text_len - 1)
                # u = Decimal(random.uniform(0.0, 1.0))
                u = random.uniform(0.0, 1.0)
                if u < q[i]:
                    text += self.alphabet[i]
                else:
                    text += self.alphabet[a[i]]
        else:
            return "Wrong mode"
        return text


def linear_search(p):
    u = random.random()
    for i in range(len(p)):
        u = u - p[i]
        if u < 0:
            return i
    return len(p)


def binary_search(p):
    intervals = [sum(p[0:i]) for i in range(len(p) + 1)]
    u = random.random()
    left = 0
    right = len(p)
    m = 0
    while left <= right:
        m = (left + right) // 2
        if intervals[left] <= u < intervals[right] and left + 1 == right:
            return left
        elif intervals[m] <= u:
            left = m
        elif intervals[m] > u:
            right = m
    return m


def alias_table(p):
    m = len(p)
    p = normalized(p, target=m, positive=True)
    p = tuple(p)
    light = [(i, prob) for (i, prob) in enumerate(p) if prob <= 1]
    heavy = [(i, prob) for (i, prob) in enumerate(p) if prob > 1]
    a, q = [None] * m, [None] * m
    while len(heavy) > 0:
        i, pi = light.pop()
        j, pj = heavy.pop()
        a[i], q[i] = j, pi
        pj -= (1 - pi)
        lj = light if pj <= 1 else heavy
        lj.append((j, pj))
    for (i, prob) in light:
        a[i], q[i] = i, 1.0
    return a, q


def normalized(p, target, positive):
    if positive:
        if any(p < 0):
            return False
    sump = sum(p)
    return p * target / sump
