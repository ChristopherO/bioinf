import numpy as np
from math import log, exp, log1p
import matplotlib.pyplot as plt
import random
import sys

def simulate(transition_prob, emission_prob):
    """
    
    Args:
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
    """
    for i in range(100):
        filename = "simulation" + str(i) + ".txt"
        observations = simulate_throw(transition_prob, emission_prob)
        with open(filename, 'w') as f:
            # f.write("state observation\n")
            for state, ob in observations:
                f.write(str(state) + " " + str(ob) + "\n")

def simulate_throw(transition_prob, emission_prob):
    """
    
    Args:
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
    
    returns:
        A list of tuples of states and observations.
    """
    observations = []
    # observations = []
    state = 0 # begin with fair cube
    observations.append((state, linearsearch(emission_prob[state])))
    for i in range(999):
        state = linearsearch(transition_prob[state])
        observations.append((state, linearsearch(emission_prob[state])))
    return observations

def linearsearch(p):
    """Linear search algorithm for the next symbol.
    
    Args:
        p (ndarray(float)): The distribution
    
    Returns:
        int: The index of the simulated symbol.
    """
    U = random.random()
    for i in range(len(p)):
        U = U - p[i]
        if U < 0:
            return i
    return 0

def load_simulations():
    simulations = []
    for i in range(100):
        states = []
        observations = []
        filename = "simulation" + str(i) + ".txt"
        with open(filename, 'r') as f:
            for line in f:
                line = line.rstrip()
                ob = line.split(" ")
                states.append(int(ob[0]))
                observations.append(int(ob[1]))
        simulations.append((states, observations))
    return simulations

def log_scaled(init, transition_prob, emission_prob):
    log_init = np.zeros(init.shape)
    log_trans = np.zeros(transition_prob.shape)
    log_emission = np.zeros(emission_prob.shape)
    for i in range(len(transition_prob)):
        log_init[i] = log(init[i]) if init[i] != 0.0 else sys.float_info.min
        for j in range(len(transition_prob)):
            log_trans[i, j] = log(transition_prob[i, j]) if transition_prob[i, j] != 0.0 else sys.float_info.min
        for k in range(len(emission_prob[i])):
            log_emission[i, k] = log(emission_prob[i, k]) if emission_prob[i, k] != 0.0 else sys.float_info.min
    return (log_init, log_trans, log_emission)

def forward_simulations(simulations, init, transition_prob, emission_prob, states, lg = False):
    """Computes the forward algorithm for all simulations.
    
    Args:
        simulations (list(tuple(list, list))): The simulated throws
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
        states (list[int]): The states
        lg (bool, optional): True for logarithmic scaling
    
    Returns:
        A list of the probabilities of the observations of all simulations.
    """
    # results = []
    # for _, ob in simulations:
        # results.append(forward_algorithm(transition_prob, emission_prob, states, ob, lg))
    # return results
    if lg:
        return [scaled_forward_algorithm(init, transition_prob, emission_prob, states, ob) for _, ob in simulations]
    else:
        return [forward_algorithm(init, transition_prob, emission_prob, states, ob) for _, ob in simulations]

def forward_algorithm(init, transition_prob, emission_prob, states, observations):
    """Implements the forward algorithm for HMMs.
    
    Args:
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
        states (list[int]): The states
        observations (list(int)): The observations
    
    Returns:
        The probability of the observations.
    """
    fw = np.zeros((len(states), len(observations)))
    for i in range(len(states)):
        fw[i, 0] = init[i] * emission_prob[i, observations[0]]
    for t in range(1, len(observations)):
        for i in range(len(states)):
            fw[i, t] = sum((fw[j, t - 1] * transition_prob[j, i]) for j in range(len(states))) * emission_prob[i, observations[t]]
    return sum((fw[i, len(observations) - 1]) for i in range(len(states)))

def compute_cvalue(alpha, states):
    alpha_sum = 0.0
    for y in states:
        alpha_sum += alpha[y]
    if alpha_sum == 0:
        # given that the initial prob in the base case at least is non zero we dont expect alpha_sum to become zero
        print("Critical Error, sum of alpha values is zero")
    cval = 1.0 / alpha_sum
    if cval == 0:
        print("ERROR cval is zero, alpha = ", alpha_sum)
    return cval
    
def scaled_forward_algorithm(init, transition_prob, emission_prob, states, observations):
    """Implements the forward algorithm for HMMs.
    
    Args:
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
        states (list[int]): The states
        observations (list(int)): The observations
    
    Returns:
        The probability of the observations.
    """
    fwd = np.zeros((len(observations), len(states)))
    local_alpha = {}
    clist = []
    fwd_scaled = np.zeros((len(observations), len(states)))
    # Initialize base cases (t == 0)
    for y in states:
        fwd[0, y] = init[y] * emission_prob[y, observations[0]]

    # get c1 for base case
    c1 = compute_cvalue(fwd[0], states)
    clist.append(c1)
    # create scaled alpha values
    for y in states:
        fwd_scaled[0, y] = c1 * fwd[0, y]
        
    for t in range(1, len(observations)):
        for y in states:
            local_alpha[y] = sum((fwd_scaled[t-1, y0] * transition_prob[y0, y] * emission_prob[y, observations[t]]) for y0 in states)
        c1 = compute_cvalue(local_alpha, states)
        clist.append(c1)
        for y in states:
            fwd_scaled[t, y] = c1 * local_alpha[y]
    log_p = -sum([log(c) for c in clist])
    
    return log_p

def viterbi_simulations(simulations, init, transition_prob, emission_prob, states, lg = False):
    """Computes the viterbi algorithm for all simulations.
    
    Args:
        simulations (list(tuple(list, list))): The simulated throws
        transition_prob (ndarray): The transition matrix
        emission_prob (ndarray): The emission probabilities
        states (list[int]): The states
        lg (bool, optional): True for logarithmic scaling
    
    Returns:
        A list of the probabilities of the observations of all simulations.
    """
    return [viterbi_algorithm(init, transition_prob, emission_prob, states, ob, lg) for _, ob in simulations]

def viterbi_algorithm(init, transition_prob, emission_prob, states, observations, lg = False):
    """Implements the viterbi algorithm.
    
    Args:
    
    Returns:
        A tuple of viterbi-probability and viterbi-path.
    """
    vit = np.zeros((len(states), len(observations)))
    path = {}
    for i in states:
        if lg:
            vit[i, 0] = init[i] + emission_prob[i, observations[0]]
        else:
            vit[i, 0] = init[i] * emission_prob[i, observations[0]]
        path[i] = [i]
    for t in range(1, len(observations)):
        newpath = {}
        for i in states:
            if lg:
                (prob, state) = max((vit[j, t - 1] + transition_prob[j, i] + emission_prob[i, observations[t]], j) for j in range(len(states)))
            else:
                (prob, state) = max((vit[j, t - 1] * transition_prob[j, i] * emission_prob[i, observations[t]], j) for j in range(len(states)))
            vit[i, t] = prob
            newpath[i] = path[state] + [i]
        path = newpath
    (prob, state) = max((vit[i, len(observations) - 1], i) for i in states)
    return (prob, path[state])

def compare_paths(real_states, viterbi_path):
    return len([i == j for i, j in zip(real_states, viterbi_path)])


states = range(3)

prob_f = np.full((6), 1 / 6)
prob_u = np.full((6), 1 / 5)
prob_u[5] = 0.0
prob_a = np.full((6), 1 / 7)
prob_a[5] = 2 / 7
emission_matrix = np.array([prob_f, prob_u, prob_a])

init = np.array([1.0, 0.0, 0.0])

transition_matrix = np.array([[0.8, 0.1, 0.1],
                              [0.2, 0.8, 0.0],
                              [0.2, 0.0, 0.8]])

simulate(transition_matrix, emission_matrix)

simulations = load_simulations()

results = forward_simulations(simulations, init, transition_matrix, emission_matrix, states, True)

init, transition_matrix, emission_matrix = log_scaled(init, transition_matrix, emission_matrix)

# print(results)

plt.hist(results)
plt.xlabel('Wahrscheinlichkeiten')
plt.ylabel('Häufigkeiten')
plt.title('Wahrscheinlichkeiten der Beobachtungsreihen')
## plt.legend(loc='upper left')
plt.show()

# print(simulations)
viterbi_paths = viterbi_simulations(simulations, init, transition_matrix, emission_matrix, states, True)
# print(viterbi_paths)

print([compare_paths(simulations[i][0], viterbi_paths[i][1]) for i in range(len(viterbi_paths))])